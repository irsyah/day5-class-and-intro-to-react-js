```JavaScript
let books = [
    { 
        id: 1,
        title: `Harry Potter and the Sorcerer's Stone`, 
        desc: `Harry Potter has no idea how famous he is. That's because he's being raised by his miserable aunt and uncle who are terrified Harry will learn that he's really a wizard, just as his parents were. But everything changes when Harry is summoned to attend an infamous school for wizards, and he begins to discover some clues about his illustrious birthright. From the surprising way he is greeted by a lovable giant, to the unique curriculum and colorful faculty at his unusual school, Harry finds himself drawn deep inside a mystical world he never knew existed and closer to his own noble destiny.`, 
        coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51HSkTKlauL._SX346_BO1,204,203,200_.jpg', 
        stocks: 3,
        author: `J.K Rowling`
    },
    { 
        id: 2
        title: `Artemis Fowl: The Arctic Incident`, 
        desc: `Artemis Fowl receives an urgent e-mail from Russia. In it is a plea from a man who has been kidnapped by the Russian Mafiya: his father. As Artemis rushes to his rescue, he is stopped by a familiar nemesis, Captain Holly Short of the LEPrecon Unit. Now, instead of battling the fairies, Artemis must join forces with them if he wants to save one of the few people in the world he loves.`, 
        coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51zkTXeUIwL._SX338_BO1,204,203,200_.jpg', 
        stocks: 1
        author: `Eoin Colfer`
    },
    { 
        id: 3
        title: `Pulang`, 
        desc: `Aku tahu sekarang, lebih banyak luka di hati bapakku dibanding di tubuhnya. Juga mamakku, lebih banyak tangis di hati Mamak dibanding di matanya.Sebuah kisah tentang perjalanan pulang, melalui pertarungan demi pertarungan, untuk memeluk erat semua kebencian dan rasa sakit.`, 
        coverImage: 'https://www.bukukita.com/babacms/displaybuku/106055_f.jpg', 
        stocks: 0
        author: `Tere Liye`
    }
]
```

- Tampilkan data books menggunakan React JS
- Setiap book memilik tombol "buy"
- saat menekan tombol "buy" munculkan Alert "<nama_buku> berhasil dibeli"
- Jika stock sama dengan 0 maka tombol "buy" tidak tampil