class Animal {
    //constructor
    constructor(kaki) {
        this._kaki = kaki || 4;
        this._mata = 2;
    }

    // setter
    set kaki(value) {
        this._kaki = value;
    }

    set mata(value) {
        this._mata = value;
    }

    // getter
    get kaki() {
        return this._kaki;
    }

    get mata() {
        return this._mata;
    }

    berjalan() {
        return 'berlari';
    }
}

let kucing = {
    kaki: 4,
    berjalan: () => {
        return 'kucing berjalan';
    }
};

let kuda = new Animal();

console.log(kucing);
console.log(kucing.berjalan());

// kuda.mata = 4;

console.log(kuda);
console.log(kuda.mata);
console.log(kuda.berjalan());

class Kelinci extends Animal {
    constructor() {
        super();
        this._kaki = 2; // override data parent
    }

    berjalan() { //override method parent
        return 'melompat';
    }
}

let rabbit = new Kelinci();

console.log(rabbit.berjalan());



// 4 KARAKTERISTIK OOP
// 1. ENKAPSULASI
        // di JS tidak ada private, public ataupun protected
        // jadi tidak 100% diterapkan di JS
// 2. INHERITANCE

// 3. POLYMORPHISM
    // - OVERRIDE // replace property /method parent
    // - OVERLOADING // satu nama method itu bisa lebih dari satu tetapi parameter nya yang berbeda baik itu jumlah ataupun tipe datanya
    // TIDAK 100% diterapkan

// 4. ABSTRACTION


// class Hospital {
//     constructor() {
//         this.employment = []
//     }
// }

// class Employment {
// }

// class Doctor extends Employment {
// }

// class Suster extends Employment {

// }

// class Book {
//     constructor() {
//         this.isbn = 
//         this.title
//         this.author
//     }
// }

// class Author {
//     constructor() {
//         this.books = []
//     }

//     add_books(book) {
//         this.books.push(book)
//     }
// }

// let jkRowling = new Author();

// let harryPotter1 = new Book();
// jkRowling.add_books(harryPotter1);