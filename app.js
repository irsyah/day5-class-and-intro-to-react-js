// 1. DOM
// const app = document.getElementById('app');

// const greeting = document.createElement('h1');
// greeting.innerText = 'Hello World';

// app.appendChild(greeting);

// 2. REACT - createElementAPI
// const greeting = React.createElement('h2', {}, 'Hello World');


// ReactDOM.render(greeting, document.getElementById('app'));

// 3. REACT - JSX
let person = {
    username: 'Azizi',
    age: 16,
    gender: 'male'
}

let todos = ['Learn HTML', 'Learn CSS', 'Learn DOM', 'Learn React'];

let movies = [
    {
        id: 1,
        title: "No Time To Die",
        year: 2021,
        desc: "James Bond is enjoying a tranquil life in Jamaica after leaving active service. However, his peace is short-lived as his old CIA friend, Felix Leiter, shows up and asks for help. The mission to rescue a kidnapped scientist turns out to be far more treacherous than expected, leading Bond on the trail of a mysterious villain who's armed with a dangerous new technology",
        casts: [ "Daniel Craig", "Rami Malek", "Léa Seydoux" ],
        poster: "https://cdn1-production-images-kly.akamaized.net/3buPDf0MVwgDuGk-A63Wxi3943I=/640x853/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2987071/original/060424900_1575520835-james_bond.jpg"
    },
    {
        id: 2,
        title: "Squid Game",
        year: 2021,
        desc: "Hundreds of cash-strapped contestants accept an invitation to compete in children's games for a tempting prize, but the stakes are deadly.",
        casts: [ "Lee Jung Jae", "HoYeon Jung", "Park Hae Soo", "Yeong-su Oh" ],
        poster: "https://www.blackxperience.com/assets/blackattitude/blackstyle//squid-game-header-2-1633290690220-35-300x300.jpg"
    },
    {
        id: 3,
        title: "12 Angry Men",
        year: 1957,
        desc: "A dissenting juror in a murder trial slowly manages to convince the others that the case is not as obviously clear as it seemed in court.",
        casts: [ "Henry Fonda", "Lee J Cobb", "Jack Klugman"],
        poster: "https://i.pinimg.com/originals/40/10/ea/4010ea6e29cde26c2e4e501d14450bec.jpg"
    },
    { 
        id: 5,
        title: "Fight Club",
        year: 1999,
        desc: "Unhappy with his capitalistic lifestyle, a white-collared insomniac forms an underground fight club with Tyler, a careless soap salesman. Soon, their venture spirals down into something sinister.",
        casts: [ "Brad Pitt", "Edward Norton"],
        poster: "https://image.posterlounge.com/img/products/680000/676414/676414_poster_l.jpg"
    },
    {
        id: 6,
        title: "Gone Girl",
        year: 2014,
        desc: "Nick Dunne discovers that the entire media focus has shifted on him when his wife, Amy Dunne, mysteriously disappears on the day of their fifth wedding anniversary.",
        casts: [ "Rosemund Pike", "Ben Affleck", "Neil Patrick Harris"],
        poster: "https://m.media-amazon.com/images/M/MV5BMTM0MWVkNzYtODljYS00MmM2LTlkMTEtYzU2ZTdkYjE2NGEyXkEyXkFqcGdeQXVyMTYzMDM0NTU@._V1_.jpg"
    }
]


function clickMe(id) {
    alert('Click Click Click' + id);
}

const greeting = (
    <div>
        <h2>Hello {person['username']}</h2>

        {/* Contoh Conditional yang hanya if saja */}
        <h2>Hello {person['gender'].toLowerCase() === 'male'  && <React.Fragment>Mr.</React.Fragment>} {person.username} </h2>

        {/* Contoh Conditional yang hanya if dan else */}
        <h2>Hello {person['gender'].toLowerCase() === 'male'  ? <span>Mr.</span> : <span>Miss</span>} {person.username} </h2>

        <p>Paragraph ditulis di JSX</p>
        <img src="https://images-na.ssl-images-amazon.com/images/I/51zkTXeUIwL._SX338_BO1,204,203,200_.jpg" />
        <button onClick={() => clickMe(5)}>Click Me</button>

        { 
            todos.map((todo, i) => (
                <li key={i}>{todo}</li>
            ))
        }        

        <div className="row justify-content-center">
            {movies.map(movie => (
                <div key={movie.id} className="card col-3">
                    <img src={movie.poster} width="250px" height="250px" className="card-img-top p-3"/>
                    <div className="card-body">
                        <h5 className="card-title">{movie.title}</h5>
                        { movie.casts.map((cast, iCast) => (
                            <li key={iCast}> {cast} </li>
                        ))}
                    </div>
                </div>
            ))}
        </div>
    </div>
)



ReactDOM.render(greeting, document.getElementById('app'));